package de.biehlerjosef.unofficialeasybacklogadapter.domain;

public enum SprintStoryStatusId {
	ToDo,
	InProgress,
	Completed,
	Accepted;
}