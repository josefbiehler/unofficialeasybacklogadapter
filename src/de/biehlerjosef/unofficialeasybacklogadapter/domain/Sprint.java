package de.biehlerjosef.unofficialeasybacklogadapter.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.annotations.SerializedName;

import de.biehlerjosef.unofficialeasybacklogadapter.rest.EasyBacklog;
import de.biehlerjosef.unofficialeasybacklogadapter.rest.exception.LazyLoadingRequiresInstanceOfEasyBacklog;

public class Sprint extends Domain {
	@SerializedName("backlog_id")
	private Integer backlogId;
	private Integer iteration;
	@SerializedName("start_on")
	private String startOn;
	@SerializedName("duration_days")
	private Integer durationDays;
	@SerializedName("number_team_members")
	private Float numberTeamMembers;
	@SerializedName("explicit_velocity")
	private Float explicitVelocity;
	@SerializedName("completed_at")
	private String completedAt;
	
	private Backlog backlog;
	
	public void setBacklog(Backlog backlog) {
		this.backlog = backlog;
	}
	
	public Backlog getBacklog() {
		return backlog;
	}
	
	public Integer getBacklogId() {
		return backlogId;
	}
	public void setBacklogId(Integer backlogId) {
		this.backlogId = backlogId;
	}
	public Integer getIteration() {
		return iteration;
	}
	public void setIteration(Integer iteration) {
		this.iteration = iteration;
	}
	public String getStartOn() {
		return startOn;
	}
	public void setStartOn(String startOn) {
		this.startOn = startOn;
	}
	public Integer getDurationDays() {
		return durationDays;
	}
	public void setDurationDays(Integer durationDays) {
		this.durationDays = durationDays;
	}
	public Float getNumberTeamMembers() {
		return numberTeamMembers;
	}
	public void setNumberTeamMembers(Float numberTeamMembers) {
		this.numberTeamMembers = numberTeamMembers;
	}
	public Float getExplicitVelocity() {
		return explicitVelocity;
	}
	public void setExplicitVelocity(Float explicitVelocity) {
		this.explicitVelocity = explicitVelocity;
	}
	public String getCompletedAt() {
		return completedAt;
	}
	public void setCompletedAt(String completedAt) {
		this.completedAt = completedAt;
	}

	public List<SprintStory> getSprintStories() throws LazyLoadingRequiresInstanceOfEasyBacklog {
		if(!EasyBacklog.hasInstance()) {
			throw new LazyLoadingRequiresInstanceOfEasyBacklog();
		}
		List<SprintStory> sprintStories = EasyBacklog.getEasyBacklog().getSprintStories(getBacklog());
		ArrayList<SprintStory> retVal = new ArrayList<SprintStory>();
		for(SprintStory ss : sprintStories) {
			if (ss.getSprint().equals(this)) {
				retVal.add(ss);
			}
		}
		return retVal;
	}
}
