package de.biehlerjosef.unofficialeasybacklogadapter.domain;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

import de.biehlerjosef.unofficialeasybacklogadapter.rest.EasyBacklog;
import de.biehlerjosef.unofficialeasybacklogadapter.rest.exception.LazyLoadingRequiresInstanceOfEasyBacklog;

public class Backlog extends Domain {
    private String name;
    @SerializedName("account_id")
    private Integer accountId;
    @SerializedName("company_id")
    private Integer companyId;
    private Float velovity;
    private Integer rate;
    @SerializedName("use_50_50")
    private Boolean use5050;
    @SerializedName("scoring_rule_id")
    private Integer scoringRuleId;
    private Boolean archived;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getAccountId() {
        return accountId;
    }
    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }
    public Integer getCompanyId() {
        return companyId;
    }
    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }
    public Float getVelovity() {
        return velovity;
    }
    public void setVelovity(Float velovity) {
        this.velovity = velovity;
    }
    public Integer getRate() {
        return rate;
    }
    public void setRate(Integer rate) {
        this.rate = rate;
    }
    public Boolean getUse5050() {
        return use5050;
    }
    public void setUse5050(Boolean use5050) {
        this.use5050 = use5050;
    }
    public Integer getScoringRuleId() {
        return scoringRuleId;
    }
    public void setScoringRuleId(Integer scoringRuleId) {
        this.scoringRuleId = scoringRuleId;
    }
    public Boolean getArchived() {
        return archived;
    }
    public void setArchived(Boolean archived) {
        this.archived = archived;
    }

    public List<Theme> getThemes() throws LazyLoadingRequiresInstanceOfEasyBacklog {
    	if (!EasyBacklog.hasInstance()) {
    		throw new LazyLoadingRequiresInstanceOfEasyBacklog();
    	}
    	return EasyBacklog.getEasyBacklog().getThemes(this);
    }
    
    public List<Sprint> getSprints() throws LazyLoadingRequiresInstanceOfEasyBacklog {
    	if(!EasyBacklog.hasInstance()) {
    		throw new LazyLoadingRequiresInstanceOfEasyBacklog();
    	}
    	return EasyBacklog.getEasyBacklog().getSprints(this);
    }
    
    public List<Story> getStories() throws LazyLoadingRequiresInstanceOfEasyBacklog {
    	if (!EasyBacklog.hasInstance()) {
    		throw new LazyLoadingRequiresInstanceOfEasyBacklog();
    	}
    	List<Story> stories = new ArrayList<Story>();
    	for(Theme t :  EasyBacklog.getEasyBacklog().getThemes(this)) {
    		stories.addAll(EasyBacklog.getEasyBacklog().getStories(t));
    	}
    	return stories;
    }
}
