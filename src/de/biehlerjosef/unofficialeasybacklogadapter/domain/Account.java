package de.biehlerjosef.unofficialeasybacklogadapter.domain;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import de.biehlerjosef.unofficialeasybacklogadapter.rest.EasyBacklog;
import de.biehlerjosef.unofficialeasybacklogadapter.rest.exception.LazyLoadingRequiresInstanceOfEasyBacklog;

public class Account extends Domain {
	private String name;
	@SerializedName("locale_id")
	private Integer LocaleId;
	@SerializedName("default_velocity")
	private Float defaultVelocity;
	@SerializedName("default_rate")
	private Integer defaultRate;
	@SerializedName("default_use_50_50")
	private Boolean defaultUse5050;
	@SerializedName("scoring_rule_id")
	private Integer scoringRuleId;

	private List<Backlog> backlogs;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getLocaleId() {
		return LocaleId;
	}
	public void setLocaleId(Integer localeId) {
		LocaleId = localeId;
	}
	public Float getDefaultVelocity() {
		return defaultVelocity;
	}
	public void setDefaultVelocity(Float defaultVelocity) {
		this.defaultVelocity = defaultVelocity;
	}
	public Integer getDefaultRate() {
		return defaultRate;
	}
	public void setDefaultRate(Integer defaultRate) {
		this.defaultRate = defaultRate;
	}
	public Boolean getDefaultUse5050() {
		return defaultUse5050;
	}
	public void setDefaultUse5050(Boolean defaultUse5050) {
		this.defaultUse5050 = defaultUse5050;
	}
	public Integer getScoringRuleId() {
		return scoringRuleId;
	}
	public void setScoringRuleId(Integer scoringRuleId) {
		this.scoringRuleId = scoringRuleId;
	}
	
	public List<Backlog> getBacklogs() throws LazyLoadingRequiresInstanceOfEasyBacklog {
		if(EasyBacklog.hasInstance()) {
			return EasyBacklog.getEasyBacklog().getBacklogs();
		}
		throw new LazyLoadingRequiresInstanceOfEasyBacklog();
	}
}
