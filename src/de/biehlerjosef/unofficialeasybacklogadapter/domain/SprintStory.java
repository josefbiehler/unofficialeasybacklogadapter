package de.biehlerjosef.unofficialeasybacklogadapter.domain;

import com.google.gson.annotations.SerializedName;

import de.biehlerjosef.unofficialeasybacklogadapter.rest.EasyBacklog;
import de.biehlerjosef.unofficialeasybacklogadapter.rest.exception.LazyLoadingRequiresInstanceOfEasyBacklog;

public class SprintStory extends Domain {
	@SerializedName("sprint_id")
	private Integer sprintId;
	@SerializedName("story_id")
	private Integer storyId;
	@SerializedName("sprint_story_status_id")
	private SprintStoryStatusId sprintStoryStatusId;
	private Integer position;
	
	private Sprint sprint;
	public void setSprint(Sprint sprint) {
		this.sprint = sprint;
	}
	public Sprint getSprint() {
		return sprint;
	}
	
	public Integer getSprintId() {
		return sprintId;
	}
	public void setSprintId(Integer sprintId) {
		this.sprintId = sprintId;
	}
	public Integer getStoryId() {
		return storyId;
	}
	public void setStoryId(Integer storyId) {
		this.storyId = storyId;
	}
	public SprintStoryStatusId getSprintStoryStatusId() {
		return sprintStoryStatusId;
	}
	public void setSprintStoryStatusId(SprintStoryStatusId sprintStoryStatusId) {
		this.sprintStoryStatusId = sprintStoryStatusId;
	}
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}
	
	public Story getStory() throws LazyLoadingRequiresInstanceOfEasyBacklog {
		if(!EasyBacklog.hasInstance()) {
			throw new LazyLoadingRequiresInstanceOfEasyBacklog();
		}
		return EasyBacklog.getEasyBacklog().getStory(getStoryId(), getStory().getTheme());
	}
}
