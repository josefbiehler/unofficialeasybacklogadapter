package de.biehlerjosef.unofficialeasybacklogadapter.domain;

import com.google.gson.annotations.SerializedName;

public class Company extends Domain {
	@SerializedName("default_velocity")
	private Float defaultVelocity;
	@SerializedName("default_rate")
	private Integer defaultRate;
	@SerializedName("default_use_50_50")
	private Boolean defaultUse5050;
	private String name;
	public Float getDefaultVelocity() {
		return defaultVelocity;
	}
	public void setDefaultVelocity(Float defaultVelocity) {
		this.defaultVelocity = defaultVelocity;
	}
	public Integer getDefaultRate() {
		return defaultRate;
	}
	public void setDefaultRate(Integer defaultRate) {
		this.defaultRate = defaultRate;
	}
	public Boolean getDefaultUse5050() {
		return defaultUse5050;
	}
	public void setDefaultUse5050(Boolean defaultUse5050) {
		this.defaultUse5050 = defaultUse5050;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
