package de.biehlerjosef.unofficialeasybacklogadapter.domain;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import de.biehlerjosef.unofficialeasybacklogadapter.rest.EasyBacklog;
import de.biehlerjosef.unofficialeasybacklogadapter.rest.exception.LazyLoadingRequiresInstanceOfEasyBacklog;

public class Story extends Domain {
	@SerializedName("theme_id")
	private Integer themeId;
	@SerializedName("unique_id")
	private Integer uniqueId;
	@SerializedName("as_a")
	private String asA;
	@SerializedName("i_want_to")
	private String iWantTo;
	@SerializedName("so_i_can")
	private String soICan;
	private String comments;
	private Float score;
	@SerializedName("score_50")
	private Float score50;
	@SerializedName("score_90")
	private Float score90;
	private String color;
	private Integer position;
	
	private Theme theme;
	public void setTheme(Theme theme) {
		this.theme = theme;
	}
	public Theme getTheme() {
		return theme;
	}
	
	public Integer getThemeId() {
		return themeId;
	}
	public void setThemeId(Integer themeId) {
		this.themeId = themeId;
	}
	public Integer getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(Integer uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getAsA() {
		return asA;
	}
	public void setAsA(String asA) {
		this.asA = asA;
	}
	public String getiWantTo() {
		return iWantTo;
	}
	public void setiWantTo(String iWantTo) {
		this.iWantTo = iWantTo;
	}
	public String getSoICan() {
		return soICan;
	}
	public void setSoICan(String soICan) {
		this.soICan = soICan;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Float getScore() {
		return score;
	}
	public void setScore(Float score) {
		this.score = score;
	}
	public Float getScore50() {
		return score50;
	}
	public void setScore50(Float score50) {
		this.score50 = score50;
	}
	public Float getScore90() {
		return score90;
	}
	public void setScore90(Float score90) {
		this.score90 = score90;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}
	
	public Sprint getSprint() throws LazyLoadingRequiresInstanceOfEasyBacklog {
		if(!EasyBacklog.hasInstance()) {
			throw new LazyLoadingRequiresInstanceOfEasyBacklog();
		}
		List<Sprint> sprints = EasyBacklog.getEasyBacklog().getSprints(this.getTheme().getBacklog());
		for(Sprint s : sprints) {
			List<SprintStory> sprintStories = s.getSprintStories();
			for(SprintStory ss : sprintStories) {
				if(ss.getStory().equals(this)) {
					return ss.getSprint();
				}
			}
		}
		return null;
	}
	
	/**
	 * Will find the very first SprintStory that assignes this story to a sprint.
	 * If more exist, they will be ignored
	 * @return
	 * @throws LazyLoadingRequiresInstanceOfEasyBacklog
	 */
	public SprintStory getSprintStory() throws LazyLoadingRequiresInstanceOfEasyBacklog {
		if(!EasyBacklog.hasInstance()) {
			throw new LazyLoadingRequiresInstanceOfEasyBacklog();
		}
		List<Sprint> sprints = EasyBacklog.getEasyBacklog().getSprints(this.getTheme().getBacklog());
		for(Sprint s : sprints) {
			List<SprintStory> sprintStories = s.getSprintStories();
			for(SprintStory ss : sprintStories) {
				if(ss.getStory().equals(this)) {
					return ss;
				}
			}
		}
		return null;
	}
}
