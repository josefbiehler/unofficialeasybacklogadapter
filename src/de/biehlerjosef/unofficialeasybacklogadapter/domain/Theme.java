package de.biehlerjosef.unofficialeasybacklogadapter.domain;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import de.biehlerjosef.unofficialeasybacklogadapter.rest.EasyBacklog;
import de.biehlerjosef.unofficialeasybacklogadapter.rest.exception.LazyLoadingRequiresInstanceOfEasyBacklog;

public class Theme extends Domain {
	@SerializedName("backlog_id")
	private Integer backlogId;
	private String name;
	private String code;
	private Integer position;
	
	private Backlog backlog;
	
	public void setBacklog(Backlog backlog) {
		this.backlog = backlog;
	}
	
	public Backlog getBacklog() {
		return backlog;
	}
	public Integer getBacklogId() {
		return backlogId;
	}
	public void setBacklogId(Integer backlogId) {
		this.backlogId = backlogId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}

	public List<Story> getStories() throws LazyLoadingRequiresInstanceOfEasyBacklog {
		if (!EasyBacklog.hasInstance()) {
			throw new LazyLoadingRequiresInstanceOfEasyBacklog();
		}
		return EasyBacklog.getEasyBacklog().getStories(this);
	}
}
