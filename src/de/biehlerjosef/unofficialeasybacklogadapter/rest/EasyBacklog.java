package de.biehlerjosef.unofficialeasybacklogadapter.rest;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import de.biehlerjosef.unofficialeasybacklogadapter.domain.Account;
import de.biehlerjosef.unofficialeasybacklogadapter.domain.Backlog;
import de.biehlerjosef.unofficialeasybacklogadapter.domain.Sprint;
import de.biehlerjosef.unofficialeasybacklogadapter.domain.SprintStory;
import de.biehlerjosef.unofficialeasybacklogadapter.domain.Story;
import de.biehlerjosef.unofficialeasybacklogadapter.domain.Theme;
import de.biehlerjosef.unofficialeasybacklogadapter.rest.exception.ParameterNotSetException;

public class EasyBacklog implements IEasyBacklog {

	private String url = null;
	private String apiKey = null;
	private Integer accountId = null;
	private static EasyBacklog easyBacklog = null;
	private IHttpAdapter httpAdapter = null;
	
	public static boolean hasInstance() {
		return easyBacklog != null;
	}
	
	public EasyBacklog() {
		httpAdapter = new HttpAdapter();
	}
	
	public EasyBacklog(IHttpAdapter httpAdapter) {
		this.httpAdapter = httpAdapter;
	}

	public static EasyBacklog getEasyBacklog() {
		if (easyBacklog == null) {
			easyBacklog = new EasyBacklog();
		}
		return easyBacklog;
	}
	
	public static EasyBacklog getEasyBacklog(IHttpAdapter httpAdapter) {
		if (easyBacklog == null) {
			easyBacklog = new EasyBacklog(httpAdapter);
		}
		return easyBacklog;
	}
	
	@Override
	public void setBaseUrl(String base) {
		url = base;
	}

	@Override
	public String getBaseUrl() {
		return url;
	}

	private void throwIfParametersMissing() {
		if (url == null || apiKey == null || accountId == null) {
			throw new ParameterNotSetException("some parameter has not been set");
		}
	}

	@Override
	public void setApiKey(String key) {
		apiKey = key;
	}

	@Override
	public String getApiKey() {
		return apiKey;
	}
	
	@Override
	public boolean authenticate() {
		throwIfParametersMissing();
		String content = httpAdapter.getResult(Constants.LOGIN_PATH, getBaseUrl(), getApiKey(), String.valueOf(getAccountId()));
		Account acc = getDomainObject(content, Account.class);
		if (acc != null && getAccountId().equals(acc.getId())) {
			return true;
		}

		return false;
	}

	public List<Theme> getThemes(Backlog backlog) {
		throwIfParametersMissing();
		String content = httpAdapter.getResult(Constants.GET_THEMES, getBaseUrl(), getApiKey(), String.valueOf(backlog.getId()));
		Gson gson = new Gson();
		Type collectionType = new TypeToken<ArrayList<Theme>>(){}.getType();
		List<Theme> themes = gson.fromJson(content, collectionType);
		
		for(Theme theme : themes) {
			theme.setBacklog(backlog);
		}
		
		return themes;
	}
	
	public Theme getTheme(Backlog backlog, Integer themeId) {
		throwIfParametersMissing();
		String content = httpAdapter.getResult(Constants.GET_THEME, getBaseUrl(), getApiKey(), String.valueOf(backlog.getId()), String.valueOf(themeId));
		Theme theme = getDomainObject(content, Theme.class);
		theme.setBacklog(backlog);
		return theme;
	}
	
	public List<Backlog> getBacklogs() {
		throwIfParametersMissing();
		String content = httpAdapter.getResult(Constants.GET_BACKLOGS, getBaseUrl(), getApiKey(), String.valueOf(getAccountId()));
		Gson gson = new Gson();
		Type collectionType = new TypeToken<ArrayList<Backlog>>(){}.getType();
		List<Backlog> backlogs = gson.fromJson(content, collectionType);
		return backlogs;
	}
	
	public List<Account> getAccounts() {
		throwIfParametersMissing();
		String content = httpAdapter.getResult(Constants.GET_ACCOUNTS, getBaseUrl(), getApiKey());
		ArrayList<Account> accounts;
		Gson gson = new Gson();
		Type collectionType = new TypeToken<ArrayList<Account>>(){}.getType();
		accounts = gson.fromJson(content, collectionType);
		return accounts;
	}

	public <T> T getDomainObject(String json, Class<T> clss) {
		Gson gson = new Gson();
		T retVal = gson.fromJson(json, clss);
		return retVal;
	}

	public <T> ArrayList<T> getDomainObjectList(String json) {
		Gson gson = new Gson();
		Type collectionType = new TypeToken<ArrayList<T>>(){}.getType();
		ArrayList<T> retVal = gson.fromJson(json, collectionType);
		return retVal;
	}

	@Override
	public void setAccountId(Integer id) {
		accountId = id;
	}

	@Override
	public Integer getAccountId() {
		return accountId;
	}
	
	public List<Story> getStories(Theme theme) {
		throwIfParametersMissing();
		String content = httpAdapter.getResult(Constants.GET_STORIES, getBaseUrl(), getApiKey(), String.valueOf(theme.getId()));
		ArrayList<Story> stories;
		Gson gson = new Gson();
		Type collectionType = new TypeToken<ArrayList<Story>>(){}.getType();
		stories = gson.fromJson(content, collectionType);
		
		for(Story s : stories) {
			s.setTheme(theme);
		}
		
		return stories;
	}
	
	public List<Sprint> getSprints(Backlog backlog) {
		throwIfParametersMissing();
		String content = httpAdapter.getResult(Constants.GET_SPRINTS, getBaseUrl(), getApiKey(), String.valueOf(backlog.getId()));
		ArrayList<Sprint> sprints;
		Gson gson = new Gson();
		Type collectionType = new TypeToken<ArrayList<Sprint>>(){}.getType();
		sprints = gson.fromJson(content, collectionType);
		
		for(Sprint s : sprints) {
			s.setBacklog(backlog);
		}
		return sprints;
	}
	
	public List<SprintStory> getSprintStories(Sprint sprint) {
		throwIfParametersMissing();
		String content = httpAdapter.getResult(Constants.GET_SPRINT_STORIES, getBaseUrl(), getApiKey(), String.valueOf(sprint.getId()));
		ArrayList<SprintStory> sprints;
		Gson gson = new Gson();
		Type collectionType = new TypeToken<ArrayList<SprintStory>>(){}.getType();
		sprints = gson.fromJson(content, collectionType);
		
		for(SprintStory s : sprints) {
			s.setSprint(sprint);
		}
		return sprints;
	}
	
	public List<SprintStory> getSprintStories(Backlog backlog) {
		List<Sprint> sprints = getSprints(backlog);
		List<SprintStory> ss = new ArrayList<SprintStory>();
		
		for(Sprint sprint : sprints) {
			List<SprintStory> sprintStories = getSprintStories(sprint);
			for(SprintStory s : sprintStories) {
				s.setSprint(sprint);
			}
			ss.addAll(sprintStories);
		}
		
		return ss;
	}
	
	public Story getStory(int storyId, Theme theme) {
		throwIfParametersMissing();
		String content = httpAdapter.getResult(Constants.GET_STORY, getBaseUrl(), getApiKey(), String.valueOf(storyId));
		Story story;
		Gson gson = new Gson();
		Type collectionType = new TypeToken<ArrayList<Story>>(){}.getType();
		story = gson.fromJson(content, collectionType);
		
		story.setTheme(theme);
		return story;
	}
}
