package de.biehlerjosef.unofficialeasybacklogadapter.rest;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class HttpAdapter implements IHttpAdapter {
	public HttpGet getParameterizedGet(String path, String url, String apiKey, String... parameters) {
		StringBuilder stringBuilder = new StringBuilder("");

		stringBuilder = new StringBuilder(url);
		if (stringBuilder.charAt(stringBuilder.length()-1) != '/'
				&& path.charAt(path.length()-1) != '/') {
			stringBuilder.append('/');
		}
		if (parameters != null && parameters.length != 0) {
			stringBuilder.append(String.format(path, (Object[])parameters));
		}else{
			stringBuilder.append(path);
		}
		stringBuilder.append(".json?api_key=").append(apiKey);
		HttpGet get = new HttpGet(stringBuilder.toString());
		return get;
	}
	
	public String getResult(String path, String url, String apiKey, String... parameters) {
		HttpGet get = this.getParameterizedGet(path, url, apiKey, parameters);
		try {
			return execute(get);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
	
	public String execute(HttpGet get) throws ClientProtocolException, IOException {
		String retVal = null;
		HttpClient client = new DefaultHttpClient();
		HttpResponse response = client.execute(get);
		HttpEntity entity = response.getEntity();
		if (entity != null) {
			InputStream is = entity.getContent();
			retVal = inputStreamToString(is);
		}
		return retVal;
	}
	
	public String inputStreamToString(InputStream str) {
		int c = 0;
		StringBuilder b = new StringBuilder("");
		String retVal = null;
		try {
			while( (c = str.read()) != -1) {
				b.append((char)c);
			}
			retVal = b.toString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				str.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return retVal;
	}
}
