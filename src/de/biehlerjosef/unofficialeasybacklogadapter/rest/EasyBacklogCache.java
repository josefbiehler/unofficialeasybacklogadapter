package de.biehlerjosef.unofficialeasybacklogadapter.rest;

import java.util.HashMap;
import java.util.Map;

public class EasyBacklogCache {
	private Map<String, Object> map;
	private static EasyBacklogCache easyBacklogCache = null;
	
	public EasyBacklogCache() {
		map = new HashMap<String, Object>();
	}
	
	public EasyBacklogCache getEasyBacklogCache() {
		if (easyBacklogCache == null) {
			easyBacklogCache = new EasyBacklogCache();
		}
		return easyBacklogCache;
	}
	
	public void set(String key, Object value) {
		map.put(key, value);
	}
	
	public Object get(String key) {
		return map.get(key);
	}
}
