package de.biehlerjosef.unofficialeasybacklogadapter.rest.exception;

public class ParameterNotSetException extends RuntimeException {

	public ParameterNotSetException(String msg) {
		super(msg);
	}
}
