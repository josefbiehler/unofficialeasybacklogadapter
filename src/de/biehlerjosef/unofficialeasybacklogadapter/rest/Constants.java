package de.biehlerjosef.unofficialeasybacklogadapter.rest;

public class Constants {
	public static final String LOGIN_PATH = "accounts/%s";
	public static final String GET_ACCOUNTS = "accounts";
	public static final String GET_ACCOUNT = "accounts/%s";
	public static final String GET_BACKLOGS = "accounts/%s/backlogs";
	public static final String GET_BACKLOG = "accounts/%s/backlogs/%s";
	public static final String GET_SPRINTS = "backlogs/%s/sprints";
	public static final String GET_SPRINT = "";
	public static final String GET_STORIES = "themes/%s/stories";
	public static final String GET_STORY = "stories/%s";
	public static final String GET_SPRINT_STORIES = "sprints/%s/sprint-stories";
	public static final String GET_SPRINT_STORY = "";
	public static final String GET_COMPANIES = "";
	public static final String GET_THEMES = "backlogs/%s/themes";
	public static final String GET_THEME = GET_THEMES + "/%s";
}
