package de.biehlerjosef.unofficialeasybacklogadapter.rest;

public interface IHttpAdapter {
	String getResult(String path, String url, String apiKey, String... parameters);
}
