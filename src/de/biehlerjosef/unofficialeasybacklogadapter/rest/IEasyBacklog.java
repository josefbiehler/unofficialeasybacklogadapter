package de.biehlerjosef.unofficialeasybacklogadapter.rest;

import java.util.List;

import de.biehlerjosef.unofficialeasybacklogadapter.domain.*;

public interface IEasyBacklog {
	void setBaseUrl(String base);
	String getBaseUrl();
	void setApiKey(String key);
	String getApiKey();
	boolean authenticate();
	void setAccountId(Integer id);
	Integer getAccountId();
	List<Theme> getThemes(Backlog backlog);
	Theme getTheme(Backlog backlog, Integer themeId);
	List<? extends Domain> getBacklogs();
	List<Account> getAccounts();
	List<Story> getStories(Theme theme);
	List<Sprint> getSprints(Backlog backlog);
	List<SprintStory> getSprintStories(Sprint sprint);
	List<SprintStory> getSprintStories(Backlog backlog);
}
